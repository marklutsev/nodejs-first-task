const express = require('express');
const User = require('../models/user');
const router = express.Router();

// Обрабатываем маршрут 
router.get('/cart', (req, res) => {
    User.find({}, (err, courses) => {
        if (err) return console.log(err);   // Вывод ошибки при её наличии
        courses = courses.map(course => course.toJSON());    // Перевод объектов из ObjectId в стандартный JSON
        let rendering;  // Объявление переменной, отвечающей за отображение

        // Присваивание переменной rendering значения, в зависимости от наличия объектов в course (курсов)
        courses.length > 0 ? rendering = true : rendering = false; 
        
        res.render('cart.hbs', {
            title: 'Корзина',
            rendering: rendering,
            list: courses
        });
    });
});

module.exports = router;