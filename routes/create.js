const express = require('express');
const router = express.Router();
const urlencodedParser = require('body-parser').urlencoded({extended: false});
const Course = require('../models/course');

router.post('/course', urlencodedParser, (req, res) => {
    // получение введённых данных
    const courseName = req.body.name;
    const coursePrice = req.body.price;
    const courseUrl = req.body.url;
    Course.create({name: courseName, price: coursePrice, url: courseUrl}, (err) => {
        if (err) return console.log(err);
        res.redirect('/courses');
    });
});

module.exports = router;    // экспортируем модель курса