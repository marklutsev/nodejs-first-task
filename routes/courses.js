const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const jsonParser = express.json();
const Course = require('../models/course');	// подключаем модель Course
const User = require('../models/user');	// подключаем модель User

const urlencodedParser = bodyParser.urlencoded({extended: false});	

// Рендеринг списка курсов
router.get('/courses', (req, res) => {
	Course.find({}, (err, courses) => {
		if (err) return console.log(err);
		let rendering = false;
		if (courses.length > 0) {
			rendering = true;
		}
		res.render('courses.hbs', {
			title: 'Курсы',
			rendering: rendering,
			courses: courses.map(course => course.toJSON())
		});
	});
});

// Рендеринг какого-либо одного курса
router.get('/courses/:id', (req, res) => {
	const id = req.params.id;	// получаем id через параметр маршрута

	let course;	// объявляем переменную, содержащую курс
	Course.findOne({_id: id}, (err, course) => {
		if (err) return console.log(err);
		course = course;
	});

	res.render('course.hbs', {
		course: course
	});
});

// Редактирование какого-либо курса (открытие)
router.get('/courses/:id/edit', (req, res) => {
	const id = req.params.id;	// получаем id через параметр маршрута

	Course.findOne({_id: id}, (err, course) => {
		if (err) return console.log(err);
		res.render('course-edit.hbs', {
			title: 'Редактировать курс',
			course: course.toJSON()
		});
	});
});

// Редактирование какого-либо курса (действие)
router.post('/courses/:id/edit', urlencodedParser, (req, res) => {
	const id = req.params.id;	// получаем id через параметр маршрута

	// получаем данные, введённые в поля формы
	const course_name = req.body.name;
	const course_price = req.body.price;
	const course_url = req.body.url;

	// находим и обновляем документа (объект) с нужным id
	Course.findByIdAndUpdate({id: id}, {name: course_name, price: course_price, url: course_url}, (err) => {
		if (err) return console.log(err);
		res.redirect('/courses');
	});
});

// Удаление курса
router.get('/delete/:id', (req, res) => {
	const id = req.params.id;	// получаем id через параметр маршрута
	Course.findOneAndDelete({id: id}, (err) => {
		if (err) return console.log(err);
		res.redirect('/courses');
	});
});

module.exports = router;