const express = require('express');
const router = express.Router();

// Обрабатываем маршрут /add
router.use('/add', (req, res) => {
	res.render("add.hbs", {
		title: "Добавить курс"
	});
})

module.exports = router;