const express = require('express');
const User = require('../models/user');
const router = express.Router();

router.get('/delete/:id', (req, res) => {
	const id = req.params.id;
	User.findOneAndDelete({_id: id}, (err, result) => {
		if (err) return console.log(err);
		res.redirect('/courses');
	});
});

module.exports = router;