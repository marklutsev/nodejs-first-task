const mongoose = require('mongoose');
const express = require('express');
const expressHbs = require('express-handlebars');
const hbs = require('hbs');
const app = express();
const path = require('path');
const cors = require('cors');

mongoose.connect(`mongodb+srv://Danila:jrhrMETZ2x6smVG@cluster0.oa3oz.mongodb.net/shop`,
{ useNewUrlParser: true }, (err) => {
	if (err) return console.log(err);
    app.listen(3000, () => {
        console.log('The server is working...');
    });
});

// подключаю статические файлы
app.use(express.static('public'));

// устанавливаю движок hbs в качестве движка представлений
app.set('view engine', 'hbs');

// устанавливаю настройки для файлов layout
app.engine('hbs', expressHbs(
	{
		layoutsDir: 'views/layouts',
		defaultLayout: 'layout',
		extname: 'hbs'
	}
));

// устанавливаю каталог с частичными представлениями
hbs.registerPartials(__dirname + '/views/partials');

// подключаю все файлы маршрутов
const home = require('./routes/home');
const courses = require('./routes/courses');
const add = require('./routes/add');
const cart = require('./routes/cart');
const course = require('./routes/create');
const cartrem = require('./routes/cartrem');
const buy = require('./routes/buy');

// обработка стандартных маршрутов
app.get('/', home);
app.get('/add', add);
app.get('/courses', courses);
app.get('/cart', cart)
app.get('/courses/:id', courses);
app.get('/courses/:id/edit', courses);
app.get('/:name', buy);

// обработка маршрутов с действиями
app.post('/course', course);
app.get('/delete/:id', courses);
app.post('/courses/:id/edit?allow=true', courses);