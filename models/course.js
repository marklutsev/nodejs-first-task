const mongoose = require('mongoose');  
const Schema = mongoose.Schema; 

// Создание новой схемы курса, id которого будет создаваться автоматически (_id в MongoDB)
const CourseScheme = new Schema({
    name: String,
    url: String,
    price: Number
});

const Course = mongoose.model('Course', CourseScheme);  // Создание модели курса

module.exports = Course;    // Экспорт модели курса