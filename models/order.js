const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// создание схемы заказа 
const OrderScheme = new Schema({
    name: String,
    price: Number,
    number: Number
});