const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// создание схемы пользователя 
const UserScheme = new Schema({
    name: String,
    price: Number,
    number: Number
});

// реализую модель пользователя
const User = mongoose.model('User', UserScheme);

module.exports = User;